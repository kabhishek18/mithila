	<!-- Home -->

	<div class="home">
		
		<!-- Home Slider -->
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(<?=base_url();?>assets/images/home_slider_1.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="<?=base_url();?>assets/categories.html">Shop NOW!</a></div>
						</div>
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(<?=base_url();?>assets/images/home_slider_1.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="<?=base_url();?>assets/categories.html">Shop NOW!</a></div>
						</div>
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(<?=base_url();?>assets/images/home_slider_1.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="<?=base_url();?>assets/categories.html">Shop NOW!</a></div>
						</div>
					</div>
				</div>

			</div>
				
			<!-- Home Slider Navigation -->
			<div class="home_slider_nav home_slider_prev trans_200"><div class=" d-flex flex-column align-items-center justify-content-center"><img src="<?=base_url();?>assets/images/prev.png" alt=""></div></div>
			<div class="home_slider_nav home_slider_next trans_200"><div class=" d-flex flex-column align-items-center justify-content-center"><img src="<?=base_url();?>assets/images/next.png" alt=""></div></div>

		</div>
	</div>

	<!-- Boxes -->
	
	<div class="boxes">
		<div class="section_container">
			<div class="container">
				<div class="row">
					
					<!-- Box -->
					<div class="col-lg-4 box_col">
						<div class="box">
							<div class="box_image"><img src="<?=base_url();?>assets/images/box_1.jpg" alt=""></div>
							<div class="box_title trans_200"><a href="<?=base_url();?>assets/categories.html">summer collection</a></div>
						</div>
					</div>

					<!-- Box -->
					<div class="col-lg-4 box_col">
						<div class="box">
							<div class="box_image"><img src="<?=base_url();?>assets/images/box_2.jpg" alt=""></div>
							<div class="box_title trans_200"><a href="<?=base_url();?>assets/categories.html">eyewear collection</a></div>
						</div>
					</div>

					<!-- Box -->
					<div class="col-lg-4 box_col">
						<div class="box">
							<div class="box_image"><img src="<?=base_url();?>assets/images/box_3.jpg" alt=""></div>
							<div class="box_title trans_200"><a href="<?=base_url();?>assets/categories.html">basic pieces</a></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<!-- Products -->
	<div class="products">
		<div class="section_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="products_container grid">
							
							<?php foreach($products as $product):?>
							<!-- Product -->
							<div class="product grid-item <?=$product['type']?>">
								<div class="product_inner">
									<div class="product_image">
										<img src="<?=base_url();?>assets/images/product_1.jpg" alt="">
										<?=($product['type']==null?"":'<div class="product_tag">'.$product['type'].'</div>')?>
									</div>
									<div class="product_content text-center">
										<div class="product_title"><a href="<?=base_url();?>assets/product.html"><?=$product['name']?></a></div>
										<div class="product_price">₹ <?=$product['sellprice']?><span>₹ <?=$product['regularprice']?></span></div>
										<div class="product_button ml-auto mr-auto trans_200"><a href="<?=base_url();?>assets/#">add to cart</a></div>
									</div>
								</div>	
							</div>
							<?php endforeach?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

