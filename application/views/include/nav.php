
<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			
			<!-- Hamburger -->
			<div class="hamburger menu_mm"><i class="fa fa-bars menu_mm" aria-hidden="true"></i></div>

			<!-- Logo -->
			<div class="header_logo">
				<a href="<?=base_url();?>"><div>Mithila<span>Shop</span></div></a>
			</div>

			<!-- Navigation -->
			<nav class="header_nav">
				<ul class="d-flex flex-row align-items-center justify-content-start">
					<?php foreach($nav as $navs):?>
					<li><a href="<?=base_url();?><?=$navs['name']?>"><?=$navs['name']?></a></li>
					<?php endforeach;?>
					<li><a href="<?=base_url();?>">blog</a></li>
					<li><a href="<?=base_url();?>contact">contact</a></li>
				</ul>
			</nav>

			<!-- Header Extra -->
			<div class="header_extra ml-auto d-flex flex-row align-items-center justify-content-start">

				<!-- Cart -->
				<div class="cart d-flex flex-row align-items-center justify-content-start">
					<div class="cart_icon"><a href="<?=base_url();?>assets/cart.html">
						<img src="<?=base_url();?>assets/images/bag.png" alt="">
						<div class="cart_num">2</div>
					</a></div>
				</div>

			</div>

		</div>
	</header>

	<!-- Menu -->

	<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="menu_top d-flex flex-row align-items-center justify-content-start">

		
			
		</div>
		<div class="menu_search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav">
			<ul class="menu_mm">
				<?php foreach($nav as $navs):?>
				<li class="menu_mm"><a href="<?=base_url();?><?=$navs['name']?>"><?=$navs['name']?></a></li>
				<?php endforeach;?>
				<li class="menu_mm"><a href="<?=base_url();?>/blog">blogs</a></li>
				<li class="menu_mm"><a href="<?=base_url();?>contact">contact</a></li>
			</ul>
		</nav>
		<div class="menu_extra">
			<div class="menu_social">
				<ul>

					<li><a href="<?=base_url();?>assets/#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
					<li><a href="<?=base_url();?>assets/#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="<?=base_url();?>assets/#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="<?=base_url();?>assets/#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	
	<!-- Sidebar -->

	<div class="sidebar">
		
	
<!-- Logo -->
		<div class="sidebar_logo">
			<a href="<?=base_url();?>"><div>Mithila<br><span>shop</span></div></a>
		</div>
			

		<!-- Sidebar Navigation -->
		<nav class="sidebar_nav">
			<ul>
				<?php foreach($nav as $navs):?>
				<li><a href="<?=base_url();?><?=$navs['name']?>"><?=$navs['name']?><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<?php endforeach;?>
				<li><a href="<?=base_url();?>assets/blog.html">blog<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li><a href="<?=base_url();?>assets/#">contact<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
		</nav>

		<!-- Search -->
		<div class="search">
			<form action="#" class="search_form" id="sidebar_search_form">
				<input type="text" class="search_input" placeholder="Search" required="required">
				<button class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
		</div>

		<!-- Cart -->
		<div class="cart d-flex flex-row align-items-center justify-content-start">
			<div class="cart_icon"><a href="<?=base_url();?>assets/cart.html">
				<img src="<?=base_url();?>assets/images/bag.png" alt="">
				<div class="cart_num">2</div>
			</a></div>
			<div class="cart_text">bag</div>
			<div class="cart_price">$39.99 (1)</div>
		</div>
	</div>
