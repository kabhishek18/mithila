
	<!-- Footer -->

	<footer class="footer">
		<div class="footer_content">
			<div class="section_container">
				<div class="container">
					<div class="row">
						
						<!-- About -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_about">
								<!-- Logo -->
								<div class="footer_logo">
									<a href="<?=base_url();?>assets/#"><div>Mithila</br><span>shop</span></div></a>
								</div>
								<div class="footer_about_text">
									<p>Donec vitae purus nunc. Morbi faucibus erat sit amet congue mattis. Nullam fringilla faucibus urna, id dapibus erat iaculis ut. Integer ac sem.</p>
								</div>
								<div class="cards">
									<ul class="d-flex flex-row align-items-center justify-content-start">
										<li><a href="<?=base_url();?>assets/#"><img src="<?=base_url();?>assets/images/card_1.jpg" alt=""></a></li>
										<li><a href="<?=base_url();?>assets/#"><img src="<?=base_url();?>assets/images/card_2.jpg" alt=""></a></li>
										<li><a href="<?=base_url();?>assets/#"><img src="<?=base_url();?>assets/images/card_3.jpg" alt=""></a></li>
										<li><a href="<?=base_url();?>assets/#"><img src="<?=base_url();?>assets/images/card_4.jpg" alt=""></a></li>
										<li><a href="<?=base_url();?>assets/#"><img src="<?=base_url();?>assets/images/card_5.jpg" alt=""></a></li>
									</ul>
								</div>
							</div>
						</div>

						<!-- Questions -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_questions">
								<div class="footer_title">questions</div>
								<div class="footer_list">
									<ul>
										<li><a href="<?=base_url();?>assets/#">About us</a></li>
										<li><a href="<?=base_url();?>assets/#">Track Orders</a></li>
										<li><a href="<?=base_url();?>assets/#">Returns</a></li>
										<li><a href="<?=base_url();?>assets/#">Jobs</a></li>
										<li><a href="<?=base_url();?>assets/#">Shipping</a></li>
										<li><a href="<?=base_url();?>assets/#">Blog</a></li>
										<li><a href="<?=base_url();?>assets/#">Partners</a></li>
										<li><a href="<?=base_url();?>assets/#">Bloggers</a></li>
										<li><a href="<?=base_url();?>assets/#">Support</a></li>
										<li><a href="<?=base_url();?>assets/#">Terms of Use</a></li>
										<li><a href="<?=base_url();?>assets/#">Press</a></li>
									</ul>
								</div>
							</div>
						</div>

						<!-- Blog -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_blog">
								<div class="footer_title">blog</div>
								<div class="footer_blog_container">

									<!-- Blog Item -->
									<div class="footer_blog_item d-flex flex-row align-items-start justify-content-start">
										<div class="footer_blog_image"><a href="<?=base_url();?>assets/blog.html"><img src="<?=base_url();?>assets/images/footer_blog_1.jpg" alt=""></a></div>
										<div class="footer_blog_content">
											<div class="footer_blog_title"><a href="<?=base_url();?>assets/blog.html">what shoes to wear</a></div>
											<div class="footer_blog_date">june 06, 2018</div>
											<div class="footer_blog_link"><a href="<?=base_url();?>assets/blog.html">Read More</a></div>
										</div>
									</div>

									<!-- Blog Item -->
									<div class="footer_blog_item d-flex flex-row align-items-start justify-content-start">
										<div class="footer_blog_image"><a href="<?=base_url();?>assets/blog.html"><img src="<?=base_url();?>assets/images/footer_blog_2.jpg" alt=""></a></div>
										<div class="footer_blog_content">
											<div class="footer_blog_title"><a href="<?=base_url();?>assets/blog.html">trends this year</a></div>
											<div class="footer_blog_date">june 06, 2018</div>
											<div class="footer_blog_link"><a href="<?=base_url();?>assets/blog.html">Read More</a></div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<!-- Contact -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_contact">
								<div class="footer_title">contact</div>
								<div class="footer_contact_list">
									<ul>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>C.</span><div>Mithilashop Pvt. Ltd</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>A.</span><div>B-100, New Jankipuri, Uttam Nagar,New Delhi,110058</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>T.</span><div>+91 8010076092, +91 8010076092</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>E.</span><div>Support@mithilashop.com</div></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Social -->
		<div class="footer_social">
			<div class="section_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="footer_social_container d-flex flex-row align-items-center justify-content-between">
								<!-- Instagram -->
								<a href="<?=base_url();?>assets/#">
									<div class="footer_social_item d-flex flex-row align-items-center justify-content-start">
										<div class="footer_social_icon"><i class="fa fa-instagram" aria-hidden="true"></i></div>
										<div class="footer_social_title">instagram</div>
									</div>
								</a>
								<!-- Google + -->
								<a href="<?=base_url();?>assets/#">
									<div class="footer_social_item d-flex flex-row align-items-center justify-content-start">
										<div class="footer_social_icon"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
										<div class="footer_social_title">google +</div>
									</div>
								</a>
								<!-- Facebook -->
								<a href="<?=base_url();?>assets/#">
									<div class="footer_social_item d-flex flex-row align-items-center justify-content-start">
										<div class="footer_social_icon"><i class="fa fa-facebook" aria-hidden="true"></i></div>
										<div class="footer_social_title">facebook</div>
									</div>
								</a>
								
								<!-- YouTube -->
								<a href="<?=base_url();?>assets/#">
									<div class="footer_social_item d-flex flex-row align-items-center justify-content-start">
										<div class="footer_social_icon"><i class="fa fa-youtube" aria-hidden="true"></i></div>
										<div class="footer_social_title">youtube</div>
									</div>
								</a>
								
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Credits -->
		<div class="credits">
			<div class="section_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="credits_content d-flex flex-row align-items-center justify-content-end">
								<div class="credits_text">

									Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="http://kabhishek18.com/" target="_blank">Kabhishek18</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

<script src="<?=base_url();?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?=base_url();?>assets/styles/bootstrap-4.1.3/popper.js"></script>
<script src="<?=base_url();?>assets/styles/bootstrap-4.1.3/bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/plugins/greensock/TweenMax.min.js"></script>
<script src="<?=base_url();?>assets/plugins/greensock/TimelineMax.min.js"></script>
<script src="<?=base_url();?>assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?=base_url();?>assets/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?=base_url();?>assets/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="<?=base_url();?>assets/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="<?=base_url();?>assets/plugins/easing/easing.js"></script>
<script src="<?=base_url();?>assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="<?=base_url();?>assets/plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="<?=base_url();?>assets/plugins/Isotope/fitcolumns.js"></script>
<script src="<?=base_url();?>assets/js/custom.js"></script>
<script src="<?=base_url();?>assets/js/categories.js"></script>
</body>
</html>
