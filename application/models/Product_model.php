<?php
class Product_model extends CI_Model{
 
    // Products Data Model
	public function All_product()
	{
		$this->db->select('*');
		$this->db->from('products');
		$array = array('status' =>'0','deleted' =>'0');
		$this->db->where($array);
		$query = $this->db->get();
		if($query->num_rows() !=0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}

	}
     
}