<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('page_model');
		$this->load->model('product_model');
		if ($this->config->item('secure_site')) {
			force_ssl();
		}
	}	
	public function index()
	{

		$data['nav']=$this->page_model->Nav_model();
		$bdata['products']=$this->product_model->all_product();	
		$this->load->view('include/header');
		$this->load->view('include/nav',$data);
		$this->load->view('home',$bdata);
		$this->load->view('page/newsletter');
		$this->load->view('include/footer');
	}

// Category Page
	public function category()
	{
		$data['nav']=$this->page_model->Nav_model();
		$this->load->view('include/header');
		$this->load->view('include/nav',$data);
		$this->load->view('category');
		$this->load->view('include/footer');
	}

// Single Page
	public function product()
	{
		
		$this->load->view('page/singlepro');
		
	}	

// cart Testing area

	public function Cart()
	{
		$data = array(
        array(
                'id'      => 'sku_123ABC',
                'qty'     => 1,
                'price'   => 39.95,
                'name'    => 'T-Shirt',
                'options' => array('Size' => 'L', 'Color' => 'Red')
        ),
        array(
                'id'      => 'sku_567ZYX',
                'qty'     => 1,
                'price'   => 9.95,
                'name'    => 'Coffee Mug'
        ),
        array(
                'id'      => 'sku_965QRS',
                'qty'     => 1,
                'price'   => 29.95,
                'name'    => 'Shot Glass'
        )
			);

		$this->cart->insert($data);

		$this->load->view('cart');

	}	
		



}
