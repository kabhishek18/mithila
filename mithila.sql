-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 22, 2019 at 03:09 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mithila`
--
CREATE DATABASE IF NOT EXISTS `mithila` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mithila`;

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

DROP TABLE IF EXISTS `cat`;
CREATE TABLE `cat` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `name`, `status`, `deleted`) VALUES
(1, 'Men', '0', '0'),
(2, 'Women', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `subcatid` int(11) NOT NULL,
  `subsubcatid` int(11) NOT NULL,
  `name` text NOT NULL,
  `skuid` text NOT NULL,
  `brand` text NOT NULL,
  `size` text NOT NULL,
  `color` text NOT NULL,
  `sellprice` text NOT NULL,
  `regularprice` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL,
  `status` text NOT NULL,
  `deleted` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `catid`, `subcatid`, `subsubcatid`, `name`, `skuid`, `brand`, `size`, `color`, `sellprice`, `regularprice`, `description`, `image`, `type`, `status`, `deleted`) VALUES
(1, 1, 1, 1, 'Round Tshirt', 'sku0001', 'Levis', 'free', 'red', '1200', '1100', 'asdf', 'fasd', 'new', '0', '0'),
(2, 1, 1, 4, 'Round Tshirt', 'sku0002', 'levis', 'm', 'blue', '789', '859', 'Loreum Sip', 'jpeg', 'Sales', '0', '2'),
(3, 1, 1, 1, 'Round Tshirt', 'sku0003', 'morka', 'l', 'green', '599', '669', 'Loreum Sip', 'jpeg', 'New', '0', '3'),
(4, 1, 2, 5, 'Bell Bottom', 'sku0004', 'own', 's', 'yellow', '655', '725', 'Loreum Sip', 'jpeg', 'New', '0', '4'),
(5, 1, 10, 15, 'Leather Wallet', 'sku0005', 'own', 's', 'black', '998', '1068', 'Loreum Sip', 'jpeg', 'New', '0', '5'),
(6, 1, 10, 17, 'Rayban ', 'sku0006', 'own', 's', 'white', '878', '948', 'Loreum Sip', 'jpeg', 'New', '0', '6'),
(7, 1, 10, 14, 'Dell Laptop Bags', 'sku0007', 'own', 's', 'red', '1211', '1281', 'Loreum Sip', 'jpeg', 'New', '0', '7'),
(8, 1, 10, 14, 'Hp Laptop Bags', 'sku0008', 'levis', 's', 'blue', '999', '1069', 'Loreum Sip', 'jpeg', 'New', '0', '8'),
(9, 1, 3, 12, 'Sparx', 'sku0009', 'morka', 's', 'green', '789', '859', 'Loreum Sip', 'jpeg', 'New', '0', '9'),
(10, 1, 3, 12, 'Jimmy Choo', 'sku0010', 'own', 's', 'yellow', '456', '526', 'Loreum Sip', 'jpeg', 'New', '0', '10'),
(11, 2, 1, 1, 'Round Tshirt', 'sku0011', 'own', 's', 'black', '456', '526', 'Loreum Sip', 'jpeg', 'New', '0', '11'),
(12, 2, 1, 4, 'Round Tshirt', 'sku0012', 'own', 's', 'white', '123', '193', 'Loreum Sip', 'jpeg', 'New', '0', '12'),
(13, 2, 1, 1, 'Round Tshirt', 'sku0013', 'levis', 's', 'red', '123', '193', 'Loreum Sip', 'jpeg', 'New', '0', '13'),
(14, 2, 2, 5, 'Bell Bottom', 'sku0014', 'morka', 's', 'blue', '21', '91', 'Loreum Sip', 'jpeg', 'New', '0', '14'),
(15, 2, 10, 15, 'Leather Wallet', 'sku0015', 'own', 's', 'green', '12', '82', 'Loreum Sip', 'jpeg', 'New', '0', '15'),
(16, 2, 10, 17, 'Rayban ', 'sku0016', 'own', 's', 'yellow', '21', '91', 'Loreum Sip', 'jpeg', 'New', '0', '16'),
(17, 2, 10, 14, 'Dell Laptop Bags', 'sku0017', 'own', 's', 'black', '2311', '2381', 'Loreum Sip', 'jpeg', 'New', '0', '17'),
(18, 2, 10, 14, 'Hp Laptop Bags', 'sku0018', 'levis', 's', 'black', '121', '191', 'Loreum Sip', 'jpeg', 'New', '0', '18'),
(19, 2, 3, 12, 'Sparx', 'sku0019', 'morka', 's', 'black', '1212', '1282', 'Loreum Sip', 'jpeg', 'New', '0', '19'),
(20, 2, 3, 12, 'Jimmy Choo', 'sku0020', 'own', 's', 'black', '123', '193', 'Loreum Sip', 'jpeg', 'New', '0', '20');

-- --------------------------------------------------------

--
-- Table structure for table `subcat`
--

DROP TABLE IF EXISTS `subcat`;
CREATE TABLE `subcat` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcat`
--

INSERT INTO `subcat` (`id`, `catid`, `name`, `status`, `deleted`) VALUES
(1, 1, 'Topwear', '0', '0'),
(2, 1, 'Bottomwear', '0', '0'),
(3, 1, 'Footwear', '0', '0'),
(4, 1, 'Innerwear ', '0', '0'),
(5, 1, ' Sports', '0', '0'),
(6, 1, 'EthnicWear', '0', '0'),
(7, 1, 'Watches', '0', '0'),
(8, 1, 'Bag ,Wallets & Bagpacks', '0', '0'),
(9, 1, 'Sleepwear', '0', '0'),
(10, 1, 'Other Accessories', '0', '0'),
(11, 2, 'Topwear', '0', '0'),
(12, 2, 'Bottomwear', '0', '0'),
(13, 2, 'Footwear', '0', '0'),
(14, 2, 'Innerwear ', '0', '0'),
(15, 2, ' Sports', '0', '0'),
(16, 2, 'EthnicWear', '0', '0'),
(17, 2, 'Watches', '0', '0'),
(18, 2, 'Bag ,Wallets & Cluthes', '0', '0'),
(19, 2, 'Sleepwear', '0', '0'),
(20, 2, 'Other Accessories', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `subsubcat`
--

DROP TABLE IF EXISTS `subsubcat`;
CREATE TABLE `subsubcat` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `subcatid` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` enum('0','1') NOT NULL,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subsubcat`
--

INSERT INTO `subsubcat` (`id`, `catid`, `subcatid`, `name`, `status`, `deleted`) VALUES
(1, 1, 1, 'T-Shirts', '0', '0'),
(2, 1, 1, 'Casual Shirts', '0', '0'),
(3, 1, 1, 'Formal Shirts', '0', '0'),
(4, 1, 1, 'SweatShirts', '0', '0'),
(5, 1, 2, 'Jeans', '0', '0'),
(6, 1, 2, 'Casual Trousers', '0', '0'),
(7, 1, 2, 'Formal Trousers', '0', '0'),
(8, 1, 2, 'Shorts', '0', '0'),
(9, 1, 3, 'Sneaker', '0', '0'),
(10, 1, 3, 'Casual Shoes', '0', '0'),
(11, 1, 3, 'Formal Shoes', '0', '0'),
(12, 1, 3, 'Sandals', '0', '0'),
(13, 1, 8, 'Backpacks', '0', '0'),
(14, 1, 8, 'Laptop Bags', '0', '0'),
(15, 1, 8, 'Wallets', '0', '0'),
(16, 2, 8, 'Jewelleries', '0', '0'),
(17, 2, 8, 'Sunglasses', '0', '0'),
(18, 2, 8, 'Ties', '0', '0'),
(19, 2, 8, 'Pockets Squares', '0', '0'),
(20, 2, 8, 'Caps', '0', '0'),
(21, 2, 8, 'Hats', '0', '0'),
(22, 2, 8, 'Belts', '0', '0'),
(23, 2, 8, 'Scarfs', '0', '0'),
(24, 2, 8, 'Mufflers', '0', '0'),
(25, 2, 8, 'Gloves', '0', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cat`
--
ALTER TABLE `cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat`
--
ALTER TABLE `subcat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subsubcat`
--
ALTER TABLE `subsubcat`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
